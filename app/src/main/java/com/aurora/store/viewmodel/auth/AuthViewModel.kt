/*
 * Aurora Store
 *  Copyright (C) 2021, Rahul Kumar Patel <whyorean@gmail.com>
 *
 *  Aurora Store is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Store is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Store.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.aurora.store.viewmodel.auth

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.aurora.Constants
import com.aurora.gplayapi.data.models.AuthData
import com.aurora.gplayapi.data.providers.DeviceInfoProvider
import com.aurora.gplayapi.helpers.AuthHelper
import com.aurora.gplayapi.helpers.AuthValidator
import com.aurora.store.AccountType
import com.aurora.store.data.AuthState
import com.aurora.store.data.RequestState
import com.aurora.store.data.model.InsecureAuth
import com.aurora.store.data.network.HttpClient
import com.aurora.store.data.providers.AccountProvider
import com.aurora.store.data.providers.NativeDeviceInfoProvider
import com.aurora.store.data.providers.SpoofDeviceProvider
import com.aurora.store.data.providers.SpoofProvider
import com.aurora.store.util.Preferences
import com.aurora.store.util.Preferences.PREFERENCE_AUTH_DATA
import com.aurora.store.viewmodel.BaseAndroidViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import nl.komponents.kovenant.task
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.*

class AuthViewModel(application: Application) : BaseAndroidViewModel(application) {

    private val spoofProvider = SpoofProvider(getApplication())

    val liveData: MutableLiveData<AuthState> = MutableLiveData()

    init {
        requestState = RequestState.Init
    }

    override fun observe() {
        val signedIn = Preferences.getBoolean(getApplication(), Constants.ACCOUNT_SIGNED_IN)
        if (signedIn) {
            liveData.postValue(AuthState.Available)
            buildSavedAuthData()
        } else {
            liveData.postValue(AuthState.Unavailable) //OneScreen Changes as not logged in before dont show loggin options
            //buildSavedAuthData();//OneScreen Changes  Logged in via anonymous insecure option as Set in pref before
        }
    }

    private fun buildGooglePropAuthData(email: String, aasToken: String) {
        updateStatus("OneScreen Store Loading ...")
        task {
            fetchAvailableDevices()
        } success {
            it.forEach {
                if(it.getProperty("UserReadableName") == "Redmi 4"){
                    com.aurora.store.util.Log.e("Could not get spoof device properties")
                    buildGoogleAuthDataReq(it, email, aasToken)
                    return@forEach
                }
            }

        } fail {
            buildGoogleAuthDataReq(null, email, aasToken)
        }

    }

    fun buildGoogleAuthDataReq(propRedme4: Properties?, email: String, aasToken: String){
        updateStatus("OneScreen Store Loading ...")

        task {
            var properties = NativeDeviceInfoProvider(getApplication()).getNativeDeviceProperties()
            if(propRedme4 != null){
                for (prop in propRedme4) {
                    //println("${prop.key} = ${prop.value}")
                    properties.setProperty( prop.key as String,prop.value as String)
                }
            }
            if (spoofProvider.isDeviceSpoofEnabled())
                properties = spoofProvider.getSpoofDeviceProperties()

            return@task AuthHelper.build(email, aasToken, properties)
        } success {
            verifyAndSaveAuth(it, AccountType.GOOGLE)
        } fail {
            updateStatus("Failed to generate Session")
        }
    }


    fun buildGoogleAuthData(email: String, aasToken: String) {
        buildGooglePropAuthData(email,aasToken)
    }



fun buildAnonymousAuthData() {
        val insecure = Preferences.getBoolean(
            getApplication(),
            Preferences.PREFERENCE_INSECURE_ANONYMOUS
        )

        if (insecure) {
            buildInSecureAnonymousAuthData()
        } else {
            buildSecureAnonymousAuthData()
        }
    }

    private fun buildSecureAnonymousAuthData() {
        updateStatus("OneScreen Store Loading ...")

        task {
            var properties = NativeDeviceInfoProvider(getApplication())
                .getNativeDeviceProperties()
           /* properties.setProperty("UserReadableName","Redmi 4");
            properties.setProperty("Build.HARDWARE","qcom");
            properties.setProperty("Build.RADIO","MPSS.TA.2.3.c1-00395-8953_GEN_PACK-1_V048");
            properties.setProperty("Build.BOOTLOADER","unknown")
            properties.setProperty("Build.FINGERPRINT","Xiaomi/santoni/santoni:7.1.2/N2G47H/V9.5.10.0.NAMMIFD:user/release-keys")
            properties.setProperty("Build.BRAND","Xiaomi")
            properties.setProperty("Build.DEVICE","santoni")
            properties.setProperty("Build.VERSION.SDK_INT","29")
            properties.setProperty("Build.MODEL","Redmi 4")
            properties.setProperty("Build.MANUFACTURER","Xiaomi")
            properties.setProperty("Build.PRODUCT","havoc_santoni")
            properties.setProperty("Build.ID","QQ3A.200805.001")
            properties.setProperty("Build.VERSION.RELEASE","10")
            properties.setProperty("TouchScreen","3")
            properties.setProperty("Keyboard","1")
            properties.setProperty("Navigation","1")
            properties.setProperty("ScreenLayout","3")
            properties.setProperty("HasHardKeyboard","false")
            properties.setProperty("HasFiveWayNavigation","false")
            properties.setProperty("GL.Version","196610")
            properties.setProperty("Screen.Density","224")
            properties.setProperty("Screen.Width","720")
            properties.setProperty("Screen.Height","1280")
            properties.setProperty("Platforms","armeabi-v7a,armeabi")
            properties.setProperty("SharedLibraries","android.ext.services,android.ext.shared,android.hidl.base-V1.0-java,android.hidl.manager-V1.0-java,android.test.base,android.test.mock,android.test.runner,com.android.future.usb.accessory,com.android.location.provider,com.android.media.remotedisplay,com.android.mediadrm.signer,com.dsi.ant.antradio_library,com.google.android.maps,com.google.android.media.effects,com.google.widevine.software.drm,com.qti.dpmapi,com.qti.dpmframework,com.qti.snapdragon.sdk.display,com.qualcomm.qcrilhook,com.qualcomm.qti.QtiTelephonyServicelibrary,com.qualcomm.qti.audiosphere,ims-ext-common,javax.obex,org.apache.http.legacy,qti-telephony-hidl-wrapper,qti-telephony-utils")
            properties.setProperty("Features","android.hardware.audio.low_latency,android.hardware.audio.output,android.hardware.biometrics.face,android.hardware.bluetooth,android.hardware.bluetooth_le,android.hardware.camera,android.hardware.camera.any,android.hardware.camera.autofocus,android.hardware.camera.flash,android.hardware.camera.front,android.hardware.consumerir,android.hardware.faketouch,android.hardware.fingerprint,android.hardware.location,android.hardware.location.gps,android.hardware.location.network,android.hardware.microphone,android.hardware.opengles.aep,android.hardware.ram.normal,android.hardware.screen.landscape,android.hardware.screen.portrait,android.hardware.sensor.accelerometer,android.hardware.sensor.compass,android.hardware.sensor.gyroscope,android.hardware.sensor.light,android.hardware.sensor.proximity,android.hardware.sensor.stepcounter,android.hardware.sensor.stepdetector,android.hardware.telephony,android.hardware.telephony.cdma,android.hardware.telephony.gsm,android.hardware.telephony.ims,android.hardware.touchscreen,android.hardware.touchscreen.multitouch,android.hardware.touchscreen.multitouch.distinct,android.hardware.touchscreen.multitouch.jazzhand,android.hardware.usb.accessory,android.hardware.usb.host,android.hardware.vulkan.compute,android.hardware.vulkan.level,android.hardware.vulkan.version,android.hardware.wifi,android.hardware.wifi.direct,android.hardware.wifi.passpoint,android.software.activities_on_secondary_displays,android.software.adoptable_storage,android.software.app_widgets,android.software.autofill,android.software.backup,android.software.cant_save_state,android.software.companion_device_setup,android.software.connectionservice,android.software.cts,android.software.device_admin,android.software.home_screen,android.software.input_methods,android.software.live_wallpaper,android.software.managed_users,android.software.midi,android.software.picture_in_picture,android.software.print,android.software.secure_lock_screen,android.software.sip,android.software.sip.voip,android.software.voice_recognizers,android.software.webview,android.sofware.nfc.beam")
            properties.setProperty("Locales","af,af_ZA,am,am_ET,ar,ar_EG,ar_SA,ar_XB,as,ast,az,be,be_BY,bg,bg_BG,bh_IN,bn,bs,ca,ca_ES,cs,cs_CZ,cy_GB,da,da_DK,de,de_DE,el,el_GR,en,en_AU,en_CA,en_GB,en_IN,en_US,en_XA,en_XC,es,es_ES,es_US,et,et_EE,eu,fa,fa_IR,fi,fi_FI,fil,fil_PH,fr,fr_CA,fr_FR,gl,gl_ES,gu,hi,hi_IN,hr,hr_HR,hu,hu_HU,hy,in,in_ID,is,it,it_IT,iw,iw_IL,ja,ja_JP,ka,kab_DZ,kk,km,kn,ko,ko_KR,ky,lo,lt,lt_LT,lv,lv_LV,mk,ml,mn,mr,ms,ms_MY,my,nb,nb_NO,ne,nl,nl_NL,or,pa,pa_IN,pl,pl_PL,pt,pt_BR,pt_PT,ro,ro_RO,ru,ru_RU,sc_IT,si,sk,sk_SK,sl,sl_SI,sq,sr,sr_Latn,sr_RS,sv,sv_SE,sw,sw_TZ,ta,te,th,th_TH,tr,tr_TR,uk,uk_UA,ur,uz,vi,vi_VN,zh_CN,zh_HK,zh_TW,zu,zu_ZA")
            properties.setProperty("GSF.version","203315024")
            properties.setProperty("Vending.version","82092000")
            properties.setProperty("Vending.versionString","20.9.20-all [0] [PR] 320416846")
            properties.setProperty("CellOperator","21601")
            properties.setProperty("SimOperator","21601")
            properties.setProperty("TimeZone","Europe/Budapest")
            properties.setProperty("GL.Extensions","GL_AMD_compressed_ATC_texture,GL_AMD_performance_monitor,GL_ANDROID_extension_pack_es31a,GL_APPLE_texture_2D_limited_npot,GL_ARB_vertex_buffer_object,GL_ARM_shader_framebuffer_fetch_depth_stencil,GL_EXT_EGL_image_array,GL_EXT_EGL_image_external_wrap_modes,GL_EXT_EGL_image_storage,GL_EXT_YUV_target,GL_EXT_blend_func_extended,GL_EXT_blit_framebuffer_params,GL_EXT_buffer_storage,GL_EXT_clip_control,GL_EXT_clip_cull_distance,GL_EXT_color_buffer_float,GL_EXT_color_buffer_half_float,GL_EXT_copy_image,GL_EXT_debug_label,GL_EXT_debug_marker,GL_EXT_discard_framebuffer,GL_EXT_disjoint_timer_query,GL_EXT_draw_buffers_indexed,GL_EXT_external_buffer,GL_EXT_fragment_invocation_density,GL_EXT_geometry_shader,GL_EXT_gpu_shader5,GL_EXT_memory_object,GL_EXT_memory_object_fd,GL_EXT_multisampled_render_to_texture,GL_EXT_multisampled_render_to_texture2,GL_EXT_primitive_bounding_box,GL_EXT_read_format_bgra,GL_EXT_robustness,GL_EXT_sRGB,GL_EXT_sRGB_write_control,GL_EXT_shader_framebuffer_fetch,GL_EXT_shader_io_blocks,GL_EXT_shader_non_constant_global_initializers,GL_EXT_tessellation_shader,GL_EXT_texture_border_clamp,GL_EXT_texture_buffer,GL_EXT_texture_cube_map_array,GL_EXT_texture_filter_anisotropic,GL_EXT_texture_format_BGRA8888,GL_EXT_texture_format_sRGB_override,GL_EXT_texture_norm16,GL_EXT_texture_sRGB_R8,GL_EXT_texture_sRGB_decode,GL_EXT_texture_type_2_10_10_10_REV,GL_KHR_blend_equation_advanced,GL_KHR_blend_equation_advanced_coherent,GL_KHR_debug,GL_KHR_no_error,GL_KHR_robust_buffer_access_behavior,GL_KHR_texture_compression_astc_hdr,GL_KHR_texture_compression_astc_ldr,GL_NV_shader_noperspective_interpolation,GL_OES_EGL_image,GL_OES_EGL_image_external,GL_OES_EGL_image_external_essl3,GL_OES_EGL_sync,GL_OES_blend_equation_separate,GL_OES_blend_func_separate,GL_OES_blend_subtract,GL_OES_compressed_ETC1_RGB8_texture,GL_OES_compressed_paletted_texture,GL_OES_depth24,GL_OES_depth_texture,GL_OES_depth_texture_cube_map,GL_OES_draw_texture,GL_OES_element_index_uint,GL_OES_framebuffer_object,GL_OES_get_program_binary,GL_OES_matrix_palette,GL_OES_packed_depth_stencil,GL_OES_point_size_array,GL_OES_point_sprite,GL_OES_read_format,GL_OES_rgb8_rgba8,GL_OES_sample_shading,GL_OES_sample_variables,GL_OES_shader_image_atomic,GL_OES_shader_multisample_interpolation,GL_OES_standard_derivatives,GL_OES_stencil_wrap,GL_OES_surfaceless_context,GL_OES_texture_3D,GL_OES_texture_compression_astc,GL_OES_texture_cube_map,GL_OES_texture_env_crossbar,GL_OES_texture_float,GL_OES_texture_float_linear,GL_OES_texture_half_float,GL_OES_texture_half_float_linear,GL_OES_texture_mirrored_repeat,GL_OES_texture_npot,GL_OES_texture_stencil8,GL_OES_texture_storage_multisample_2d_array,GL_OES_texture_view,GL_OES_vertex_array_object,GL_OES_vertex_half_float,GL_OVR_multiview,GL_OVR_multiview2,GL_OVR_multiview_multisampled_render_to_texture,GL_QCOM_YUV_texture_gather,GL_QCOM_alpha_test,GL_QCOM_extended_get,GL_QCOM_shader_framebuffer_fetch_noncoherent,GL_QCOM_shader_framebuffer_fetch_rate,GL_QCOM_texture_foveated,GL_QCOM_tiled_rendering")
            properties.setProperty("Roaming","mobile-notroaming")
            properties.setProperty("Client","android-google")*/
            if (spoofProvider.isDeviceSpoofEnabled())
                properties = spoofProvider.getSpoofDeviceProperties()

            val playResponse = HttpClient
                .getPreferredClient()
                .postAuth(
                    Constants.URL_DISPENSER,
                    gson.toJson(properties).toByteArray()
                )

            if (playResponse.isSuccessful) {
                return@task gson.fromJson(
                    String(playResponse.responseBytes),
                    AuthData::class.java
                )
            } else {
                when (playResponse.code) {
                    404 -> throw Exception("Server unreachable")
                    429 -> throw Exception("Oops, You are rate limited")
                    else -> throw Exception(playResponse.errorString)
                }
            }
        } success {
            //Set AuthData as anonymous
            it.isAnonymous = true
            verifyAndSaveAuth(it, AccountType.ANONYMOUS)
        } fail {
            updateStatus(it.message.toString())
        }
    }

    private fun fetchAvailableDevices(): List<Properties> {
        return  SpoofDeviceProvider.with(getApplication<Application>().applicationContext).availableDevice
    }

    fun buildInSecureAnonymousAuthData() {
        updateStatus("OneScreen Store Loading ...")
       task {
            fetchAvailableDevices()
        } success {
           it.forEach {
              if(it.getProperty("UserReadableName") == "Redmi 4"){
                  com.aurora.store.util.Log.e("Could not get spoof device properties")
                  buildInSecureAnonymousAuthDataReq(it)
                  return@forEach
              }
           }

       } fail {
           buildInSecureAnonymousAuthDataReq(null)
        }

    }

    fun buildInSecureAnonymousAuthDataReq(propRedme4: Properties?) {
        task {
            var properties = NativeDeviceInfoProvider(getApplication())
                .getNativeDeviceProperties()
            if(propRedme4 != null){
                for (prop in propRedme4) {
                    //println("${prop.key} = ${prop.value}")
                    properties.setProperty( prop.key as String,prop.value as String)
                }

            }

            /*properties.setProperty("UserReadableName","Redmi 4");
            properties.setProperty("Build.HARDWARE","qcom");
            properties.setProperty("Build.RADIO","MPSS.TA.2.3.c1-00395-8953_GEN_PACK-1_V048");
            properties.setProperty("Build.BOOTLOADER","unknown")
            properties.setProperty("Build.FINGERPRINT","Xiaomi/santoni/santoni:7.1.2/N2G47H/V9.5.10.0.NAMMIFD:user/release-keys")
            properties.setProperty("Build.BRAND","Xiaomi")
            properties.setProperty("Build.DEVICE","santoni")
            properties.setProperty("Build.VERSION.SDK_INT","29")
            properties.setProperty("Build.MODEL","Redmi 4")
            properties.setProperty("Build.MANUFACTURER","Xiaomi")
            properties.setProperty("Build.PRODUCT","havoc_santoni")
            properties.setProperty("Build.ID","QQ3A.200805.001")
            properties.setProperty("Build.VERSION.RELEASE","10")
            properties.setProperty("TouchScreen","3")
            properties.setProperty("Keyboard","1")
            properties.setProperty("Navigation","1")
            properties.setProperty("ScreenLayout","3")
            properties.setProperty("HasHardKeyboard","false")
            properties.setProperty("HasFiveWayNavigation","false")
            properties.setProperty("GL.Version","196610")
            properties.setProperty("Screen.Density","224")
            properties.setProperty("Screen.Width","720")
            properties.setProperty("Screen.Height","1280")
            properties.setProperty("Platforms","armeabi-v7a,armeabi")
            properties.setProperty("SharedLibraries","android.ext.services,android.ext.shared,android.hidl.base-V1.0-java,android.hidl.manager-V1.0-java,android.test.base,android.test.mock,android.test.runner,com.android.future.usb.accessory,com.android.location.provider,com.android.media.remotedisplay,com.android.mediadrm.signer,com.dsi.ant.antradio_library,com.google.android.maps,com.google.android.media.effects,com.google.widevine.software.drm,com.qti.dpmapi,com.qti.dpmframework,com.qti.snapdragon.sdk.display,com.qualcomm.qcrilhook,com.qualcomm.qti.QtiTelephonyServicelibrary,com.qualcomm.qti.audiosphere,ims-ext-common,javax.obex,org.apache.http.legacy,qti-telephony-hidl-wrapper,qti-telephony-utils")
            properties.setProperty("Features","android.hardware.audio.low_latency,android.hardware.audio.output,android.hardware.biometrics.face,android.hardware.bluetooth,android.hardware.bluetooth_le,android.hardware.camera,android.hardware.camera.any,android.hardware.camera.autofocus,android.hardware.camera.flash,android.hardware.camera.front,android.hardware.consumerir,android.hardware.faketouch,android.hardware.fingerprint,android.hardware.location,android.hardware.location.gps,android.hardware.location.network,android.hardware.microphone,android.hardware.opengles.aep,android.hardware.ram.normal,android.hardware.screen.landscape,android.hardware.screen.portrait,android.hardware.sensor.accelerometer,android.hardware.sensor.compass,android.hardware.sensor.gyroscope,android.hardware.sensor.light,android.hardware.sensor.proximity,android.hardware.sensor.stepcounter,android.hardware.sensor.stepdetector,android.hardware.telephony,android.hardware.telephony.cdma,android.hardware.telephony.gsm,android.hardware.telephony.ims,android.hardware.touchscreen,android.hardware.touchscreen.multitouch,android.hardware.touchscreen.multitouch.distinct,android.hardware.touchscreen.multitouch.jazzhand,android.hardware.usb.accessory,android.hardware.usb.host,android.hardware.vulkan.compute,android.hardware.vulkan.level,android.hardware.vulkan.version,android.hardware.wifi,android.hardware.wifi.direct,android.hardware.wifi.passpoint,android.software.activities_on_secondary_displays,android.software.adoptable_storage,android.software.app_widgets,android.software.autofill,android.software.backup,android.software.cant_save_state,android.software.companion_device_setup,android.software.connectionservice,android.software.cts,android.software.device_admin,android.software.home_screen,android.software.input_methods,android.software.live_wallpaper,android.software.managed_users,android.software.midi,android.software.picture_in_picture,android.software.print,android.software.secure_lock_screen,android.software.sip,android.software.sip.voip,android.software.voice_recognizers,android.software.webview,android.sofware.nfc.beam")
            properties.setProperty("Locales","af,af_ZA,am,am_ET,ar,ar_EG,ar_SA,ar_XB,as,ast,az,be,be_BY,bg,bg_BG,bh_IN,bn,bs,ca,ca_ES,cs,cs_CZ,cy_GB,da,da_DK,de,de_DE,el,el_GR,en,en_AU,en_CA,en_GB,en_IN,en_US,en_XA,en_XC,es,es_ES,es_US,et,et_EE,eu,fa,fa_IR,fi,fi_FI,fil,fil_PH,fr,fr_CA,fr_FR,gl,gl_ES,gu,hi,hi_IN,hr,hr_HR,hu,hu_HU,hy,in,in_ID,is,it,it_IT,iw,iw_IL,ja,ja_JP,ka,kab_DZ,kk,km,kn,ko,ko_KR,ky,lo,lt,lt_LT,lv,lv_LV,mk,ml,mn,mr,ms,ms_MY,my,nb,nb_NO,ne,nl,nl_NL,or,pa,pa_IN,pl,pl_PL,pt,pt_BR,pt_PT,ro,ro_RO,ru,ru_RU,sc_IT,si,sk,sk_SK,sl,sl_SI,sq,sr,sr_Latn,sr_RS,sv,sv_SE,sw,sw_TZ,ta,te,th,th_TH,tr,tr_TR,uk,uk_UA,ur,uz,vi,vi_VN,zh_CN,zh_HK,zh_TW,zu,zu_ZA")
            properties.setProperty("GSF.version","203315024")
            properties.setProperty("Vending.version","82092000")
            properties.setProperty("Vending.versionString","20.9.20-all [0] [PR] 320416846")
            properties.setProperty("CellOperator","21601")
            properties.setProperty("SimOperator","21601")
            properties.setProperty("TimeZone","Europe/Budapest")
            properties.setProperty("GL.Extensions","GL_AMD_compressed_ATC_texture,GL_AMD_performance_monitor,GL_ANDROID_extension_pack_es31a,GL_APPLE_texture_2D_limited_npot,GL_ARB_vertex_buffer_object,GL_ARM_shader_framebuffer_fetch_depth_stencil,GL_EXT_EGL_image_array,GL_EXT_EGL_image_external_wrap_modes,GL_EXT_EGL_image_storage,GL_EXT_YUV_target,GL_EXT_blend_func_extended,GL_EXT_blit_framebuffer_params,GL_EXT_buffer_storage,GL_EXT_clip_control,GL_EXT_clip_cull_distance,GL_EXT_color_buffer_float,GL_EXT_color_buffer_half_float,GL_EXT_copy_image,GL_EXT_debug_label,GL_EXT_debug_marker,GL_EXT_discard_framebuffer,GL_EXT_disjoint_timer_query,GL_EXT_draw_buffers_indexed,GL_EXT_external_buffer,GL_EXT_fragment_invocation_density,GL_EXT_geometry_shader,GL_EXT_gpu_shader5,GL_EXT_memory_object,GL_EXT_memory_object_fd,GL_EXT_multisampled_render_to_texture,GL_EXT_multisampled_render_to_texture2,GL_EXT_primitive_bounding_box,GL_EXT_read_format_bgra,GL_EXT_robustness,GL_EXT_sRGB,GL_EXT_sRGB_write_control,GL_EXT_shader_framebuffer_fetch,GL_EXT_shader_io_blocks,GL_EXT_shader_non_constant_global_initializers,GL_EXT_tessellation_shader,GL_EXT_texture_border_clamp,GL_EXT_texture_buffer,GL_EXT_texture_cube_map_array,GL_EXT_texture_filter_anisotropic,GL_EXT_texture_format_BGRA8888,GL_EXT_texture_format_sRGB_override,GL_EXT_texture_norm16,GL_EXT_texture_sRGB_R8,GL_EXT_texture_sRGB_decode,GL_EXT_texture_type_2_10_10_10_REV,GL_KHR_blend_equation_advanced,GL_KHR_blend_equation_advanced_coherent,GL_KHR_debug,GL_KHR_no_error,GL_KHR_robust_buffer_access_behavior,GL_KHR_texture_compression_astc_hdr,GL_KHR_texture_compression_astc_ldr,GL_NV_shader_noperspective_interpolation,GL_OES_EGL_image,GL_OES_EGL_image_external,GL_OES_EGL_image_external_essl3,GL_OES_EGL_sync,GL_OES_blend_equation_separate,GL_OES_blend_func_separate,GL_OES_blend_subtract,GL_OES_compressed_ETC1_RGB8_texture,GL_OES_compressed_paletted_texture,GL_OES_depth24,GL_OES_depth_texture,GL_OES_depth_texture_cube_map,GL_OES_draw_texture,GL_OES_element_index_uint,GL_OES_framebuffer_object,GL_OES_get_program_binary,GL_OES_matrix_palette,GL_OES_packed_depth_stencil,GL_OES_point_size_array,GL_OES_point_sprite,GL_OES_read_format,GL_OES_rgb8_rgba8,GL_OES_sample_shading,GL_OES_sample_variables,GL_OES_shader_image_atomic,GL_OES_shader_multisample_interpolation,GL_OES_standard_derivatives,GL_OES_stencil_wrap,GL_OES_surfaceless_context,GL_OES_texture_3D,GL_OES_texture_compression_astc,GL_OES_texture_cube_map,GL_OES_texture_env_crossbar,GL_OES_texture_float,GL_OES_texture_float_linear,GL_OES_texture_half_float,GL_OES_texture_half_float_linear,GL_OES_texture_mirrored_repeat,GL_OES_texture_npot,GL_OES_texture_stencil8,GL_OES_texture_storage_multisample_2d_array,GL_OES_texture_view,GL_OES_vertex_array_object,GL_OES_vertex_half_float,GL_OVR_multiview,GL_OVR_multiview2,GL_OVR_multiview_multisampled_render_to_texture,GL_QCOM_YUV_texture_gather,GL_QCOM_alpha_test,GL_QCOM_extended_get,GL_QCOM_shader_framebuffer_fetch_noncoherent,GL_QCOM_shader_framebuffer_fetch_rate,GL_QCOM_texture_foveated,GL_QCOM_tiled_rendering")
            properties.setProperty("Roaming","mobile-notroaming")
            properties.setProperty("Client","android-google")*/
            if (spoofProvider.isDeviceSpoofEnabled())
                properties = spoofProvider.getSpoofDeviceProperties()

            val playResponse = HttpClient
                .getPreferredClient()
                .getAuth(
                    Constants.URL_DISPENSER
                )

            val insecureAuth: InsecureAuth

            if (playResponse.isSuccessful) {
                insecureAuth = gson.fromJson(
                    String(playResponse.responseBytes),
                    InsecureAuth::class.java
                )
            } else {
                when (playResponse.code) {
                    404 -> throw Exception("Server unreachable")
                    429 -> throw Exception("Oops, You are rate limited")
                    else -> throw Exception(playResponse.errorString)
                }
            }

            val deviceInfoProvider = DeviceInfoProvider(properties, Locale.getDefault().toString())

            AuthHelper.buildInsecure(
                insecureAuth.email,
                insecureAuth.auth,
                Locale.getDefault(),
                deviceInfoProvider
            )
        } success {
            //Set AuthData as anonymous
            it.isAnonymous = true
            verifyAndSaveAuth(it, AccountType.ANONYMOUS)
        } fail {
            updateStatus(it.message.toString())
            liveData.postValue(AuthState.Unavailable)//OneScreen changes as only update text when timeout is received
        }
    }

    private fun buildSavedAuthData() {
        viewModelScope.launch(Dispatchers.IO) {
            supervisorScope {
                try {
                    //Load & validate saved AuthData
                    val savedAuthData = getSavedAuthData()

                    if (isValid(savedAuthData)) {
                        liveData.postValue(AuthState.Valid)
                        requestState = RequestState.Complete
                    } else {
                        //Generate and validate new auth
                        val type = AccountProvider.with(getApplication()).getAccountType()
                        when (type) {
                            AccountType.GOOGLE -> {
                                val email = Preferences.getString(
                                    getApplication(),
                                    Constants.ACCOUNT_EMAIL_PLAIN
                                )
                                val aasToken = Preferences.getString(
                                    getApplication(),
                                    Constants.ACCOUNT_AAS_PLAIN
                                )
                                buildGoogleAuthData(email, aasToken)
                            }
                            AccountType.ANONYMOUS -> {
                                buildAnonymousAuthData()
                            }
                        }
                    }
                } catch (e: Exception) {
                    when (e) {
                        is UnknownHostException -> updateStatus("No network")
                        is ConnectException -> updateStatus("Could not connect to server")
                        else -> updateStatus("Unknown error")
                    }
                    requestState = RequestState.Pending
                }
            }
        }
    }

    private fun getSavedAuthData(): AuthData {
        val rawAuth: String = Preferences.getString(getApplication(), PREFERENCE_AUTH_DATA)
        return if (rawAuth.isNotBlank())
            gson.fromJson(rawAuth, AuthData::class.java)
        else
            AuthData("", "")
    }

    private fun isValid(authData: AuthData): Boolean {
        return try {
            AuthValidator(authData)
                .using(HttpClient.getPreferredClient())
                .isValid()
        } catch (e: Exception) {
            false
        }
    }

    private fun verifyAndSaveAuth(authData: AuthData, type: AccountType) {
        //updateStatus("Verifying new session")
        updateStatus("OneScreen Store Loading ...")
        if (spoofProvider.isLocaleSpoofEnabled()) {
            authData.locale = spoofProvider.getSpoofLocale()
        } else {
            authData.locale = Locale.getDefault()
        }

        if (authData.authToken.isNotEmpty() && authData.deviceConfigToken.isNotEmpty()) {
            configAuthPref(authData, type, true)
            liveData.postValue(AuthState.SignedIn)
            requestState = RequestState.Complete
        } else {
            configAuthPref(authData, type, false)
            liveData.postValue(AuthState.SignedOut)
            requestState = RequestState.Pending

            updateStatus("Failed to verify session")
        }
    }

    private fun configAuthPref(authData: AuthData, type: AccountType, signedIn: Boolean) {
        if (signedIn) {
            //Save Auth Data
            Preferences.putString(
                getApplication(),
                PREFERENCE_AUTH_DATA,
                gson.toJson(authData)
            )
        }

        //Save Auth Type
        Preferences.putString(
            getApplication(),
            Constants.ACCOUNT_TYPE,
            type.name  // ANONYMOUS OR GOOGLE
        )

        //Save Auth Status
        Preferences.putBoolean(getApplication(), Constants.ACCOUNT_SIGNED_IN, signedIn)
    }

    private fun updateStatus(status: String) {
        liveData.postValue(AuthState.Status(status))
    }
}