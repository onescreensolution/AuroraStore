<div align="right">
  <img src="https://www.gnu.org/graphics/gplv3-88x31.png" alt="GPL v3 Logo" />
</div>
<div align="center">
  <img src="app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" title="OneScreenStoreLogo" alt="OneScreenStoreLogo" />
</div>

# OneScreen Store: A Google Playstore Client

**OneScreen Store** is an unofficial, FOSS client to Google's Play Store with an elegant design. Not only does OneScreen Store download, update, and search for apps like the Play Store, it also empowers the user with new features.

For those concerned with privacy, **OneScreen Store** does not require Google's proprietary framework (spyware?) to operate. It works perfectly fine with or without Google Play Services or [MicroG](https://microg.org/).

**OneScreen Store** was originally based on Sergei Yeriomin's [Yalp store](https://github.com/yeriomin/YalpStore). **OneScreen Store** v4.0 is a rewrite of version 3 in Kotlin and runs on all devices running Android 4.4+.

## Screenshots

<img src="screenshots/account.png" height="400">
<img src="screenshots/home.png" height="400">
<img src="screenshots/sidebar.png" height="400">
<img src="screenshots/spoof.png" height="400">

## Features

- Free/Libre software — Has GPLv3 licence
- Beautiful design — Built upon latest Material Design guidelines
- Anonymous accounts — You can log in and download with anonymous accounts so you don't have to use your own account
- Personal account login — You can download purchased apps or access your wishlist by using your own Google account
- [Exodus](https://exodus-privacy.eu.org/) integration — Instantly see trackers an app is hiding in its code

## Supported by

[<img src="https://fosshost.org/img/FosshostLogo.png" width="400">](https://fosshost.org/)

## Project references

<details><summary>Open Source libraries OneScreen Store uses</summary>

- [RX-Java](https://github.com/ReactiveX/RxJava)
- [ButterKnife](https://github.com/JakeWharton/butterknife)
- [OkHttp3](https://square.github.io/okhttp/)
- [Glide](https://github.com/bumptech/glide)
- [Fetch2](https://github.com/tonyofrancis/Fetch)
- [GPlayApi](https://gitlab.com/AuroraOSS/gplayapi)
- [PlayStoreApi-v2](https://github.com/whyorean/playstore-api-v2) (Deprecated! Used up till v3)

</details>

<details><summary>OneScreen Store is based on these projects</summary>

- [YalpStore](https://github.com/yeriomin/YalpStore)
- [AppCrawler](https://github.com/Akdeniz/google-play-crawler)
- [Raccoon](https://github.com/onyxbits/raccoon4)
- [SAI](https://github.com/Aefyr/SAI)

</details>
